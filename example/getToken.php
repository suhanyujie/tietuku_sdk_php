<?php
define('PATH',dirname(__FILE__).'/');
define('ROOT',dirname(PATH).'/');

include(ROOT.'v1.1.0/TieTuKu.class.php');

define('MY_ACCESSKEY', 'be2464de338b26a0d278f638b671e54065897f2e');//获取地址:http://open.tietuku.cn/manager
define('MY_SECRETKEY', 'da39a3ee5e6b4b0d3255bfef95601890afd80709');//获取地址:http://open.tietuku.cn/manager
/**
 * 构造函数
 *
 * @access public
 * @param mixed $accesskey 贴图库平台accesskey
 * @param mixed $secretkey 贴图库平台secretkey
 * @return void
 */
$ttk=new TTKClient(MY_ACCESSKEY,MY_SECRETKEY);
$photoId = 1194777;

$param = [
    'deadline'=>time()+60,
    'album'=>$photoId
];
$res = [
    'token'=>$ttk->op_Token->dealParam($param)->createToken(),
];
echo json_encode($res);
