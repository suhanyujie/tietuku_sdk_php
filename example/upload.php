<?php
    define('PATH',dirname(__FILE__).'/');
    define('ROOT',dirname(PATH).'/');

	include(ROOT.'v1.1.0/TieTuKu.class.php');

	define('MY_ACCESSKEY', 'be2464de338b26a0d278f638b671e54065897f2e');//获取地址:http://open.tietuku.cn/manager
	define('MY_SECRETKEY', 'da39a3ee5e6b4b0d3255bfef95601890afd80709');//获取地址:http://open.tietuku.cn/manager
	/**
     * 构造函数
     *
     * @access public
     * @param mixed $accesskey 贴图库平台accesskey
     * @param mixed $secretkey 贴图库平台secretkey
     * @return void
     */
	$ttk=new TTKClient(MY_ACCESSKEY,MY_SECRETKEY);
    $photoId = 1194777;
    var_dump(file_exists(ROOT.'example/static/mv.jpg'));   # $_FILES['file']['tmp_name'][0]

    $res=$ttk->uploadFile($photoId,ROOT.'example/static/mv.jpg');

    var_dump(json_decode($res,true));exit('#21-1#');


    //$res=$ttk->curlUpFile('1194777','file');

	/**
     * 上传单个文件到贴图库
     *
     * 对应API：{@link http://open.tietuku.cn/doc#upload}
     *
     * @access public
     * @param int $aid 相册ID
     * @param array $file 上传的文件。
     * @return string 如果$file!=null 返回请求接口的json数据否则只返回Token
     */
    var_dump($_FILES);
	 if(isset($_FILES['file'])){
	 	if(is_array($_FILES['file']['tmp_name'])){
	 		foreach ($_FILES['file']['tmp_name'] as $k => $v) {
                var_dump(file_exists($v));
	 			if(!empty($v)){
	 				$res=$ttk->uploadFile($photoId,$v);
	 				dump(json_decode($res));
	 			}
	 		}
	 	}else{
	 		$res=$ttk->uploadFile($photoId,$_FILES['file']['tmp_name']);
	 		dump(json_decode($res));
	 	}
	 }else{
         exit('请先上传文件');
	 	$res=$ttk->uploadFile($photoId);
	 	dump($res);
	 }
	/**
     * 上传多个文件到贴图库
     *
     * 对应API：{@link http://open.tietuku.cn/doc#upload}
     *
     * @access public
     * @param int $aid 相册ID
     * @param string $filename 文件域名字
     * @return string 如果$file!=null 返回请求接口的json数据否则只返回Token
     */
	//$res=$ttk->curlUpFile('你的相册ID','file');
	/**
     * 上传网络文件到贴图库 (只支持单个连接)
     *
     * 对应API：{@link http://open.tietuku.cn/doc#upload-url}
     *
     * @access public
     * @param int $aid 相册ID
     * @param string $fileurl 网络图片地址
     * @return string 如果$fileurl!=null 返回请求接口的json数据否则只返回Token
     */
	//$res=$ttk->uploadFromWeb('你的相册ID','网络图片地址');
	//dump(($res));




//比var_dump更友好的格式化输出 从 ThinkPHP 提取
function dump($var, $echo=true, $label=null, $strict=true) {
    $label = ($label === null) ? '' : rtrim($label) . ' ';
    if (!$strict) {
        if (ini_get('html_errors')) {
            $output = print_r($var, true);
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
        } else {
            $output = $label . print_r($var, true);
        }
    } else {
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        if (!extension_loaded('xdebug')) {
            $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
        }
    }
    if ($echo) {
        echo($output);
        return null;
    }else
        return $output;
}

