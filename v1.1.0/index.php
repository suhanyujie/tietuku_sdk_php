<form enctype="multipart/form-data" method="post" accept-charset="utf-8">
<input type="file" name="file[]" value="" placeholder="">
<input type="file" name="file[]" value="" placeholder="">
<button type="submit">上传</button>
</form>
<?php
	include_once('TieTuKu.class.php');
	define('MY_ACCESSKEY', 'be2464de338b26a0d278f638b671e54065897f2e');//获取地址:http://open.tietuku.cn/manager
	define('MY_SECRETKEY', 'da39a3ee5e6b4b0d3255bfef95601890afd80709');//获取地址:http://open.tietuku.cn/manager
	/**
	 * 构造函数
	 * 
	 * @access public
	 * @param mixed $accesskey 贴图库平台accesskey
	 * @param mixed $secretkey 贴图库平台secretkey
	 * @return void
	 */
	$ttk=new TTKClient(MY_ACCESSKEY,MY_SECRETKEY);
	/**
	 * 查询随机30张推荐的图片
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#list-getrandrec}
	 *
	 * @access public
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	$res=$ttk->getRandRec();
	//
	//
	
	/**
	 * 根据类型ID查询随机30张推荐的图片
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#list-getrandrec}
	 *
	 * @access public
	 * @param int $cid 类型ID。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getRandRecByCid(3);
	
	/**
	 * 分页查询全部图片列表 每页30张图片
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#list-getnewpic}
	 *
	 * @access public
	 * @param int $page_no 页数，默认为1。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getNewPic(1);
	/**
	 * 通过类型ID分页查询全部图片列表 每页30张图片
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#list-getnewpic}
	 *
	 * @access public
	 * @param int $cid 类型ID。
	 * @param int $page_no 页数，默认为1。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getNewPicByCid(2,10);
	/**
	 * 通过相册ID分页查询相册中的图片 每页30张图片
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#list-album}
	 *
	 * @access public
	 * @param int $aid 相册ID。
	 * @param int $page_no 页数，默认为1。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getAlbumPicByAid('相册ID',1);
	/**
	 * 查询自己收藏的图片列表
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#collect-getlovepic}
	 *
	 * @access public
	 * @param int $page_no 页数，默认为1。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getLovePic(1);
	/**
	 * 通过图片ID喜欢(收藏)图片
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#collect-addcollect}
	 *
	 * @access public
	 * @param int $id 图片ID。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->addCollect(608328);
	/**
	 * 通过图片ID取消喜欢(取消收藏)图片
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#collect-delcollect}
	 *
	 * @access public
	 * @param int $id 图片ID。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	 //$res=$ttk->delCollect(608328);
	/**
	 * 根据用户ID查询用户相册列表
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#album-get}
	 *
	 * @access public
	 * @param int $uid 用户ID
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getAlbumByUid(1);
	/**
	 * 创建相册
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#album-create}
	 *
	 * @access public
	 * @param string $albumname 相册名称。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->createAlbum('相册名称');
	/**
	 * 编辑相册
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#album-editalbum}
	 *
	 * @access public
	 * @param int $aid 相册ID。
	 * @param string $albumname 相册名称。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->editalbum('你的相册ID','相册名称');
	/**
	 * 通过相册ID删除相册(只能删除自己的相册 如果只有一个相册，不能删除)
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#album-delalbum}
	 *
	 * @access public
	 * @param int $aid 相册ID。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->delAlbum('你的相册ID');
	/**
	 * 根据 图片ID 查询相应的图片详细信息
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#pic-getonepic}
	 *
	 * @access public
	 * @param int $id 图片ID。
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getOnePicById(13557);
	/**
	 * 根据 图片find_url 查询相应的图片详细信息
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#pic-getonepic}
	 *
	 * @access public
	 * @param string $find_url 图片find_url
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getOnePicByFind_url('c55d2882a24f519f');
	/**
	 * 通过一组图片ID 查询图片信息
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#list-getpicbyids}
	 *
	 * @access public
	 * @param mix $ids 图片ID数组。(1.多个ID用逗号隔开 2.传入数组)
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getPicByIds('13557,13558');
	/**
	 * 查询所有的分类
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#catalog-getall}
	 *
	 * @access public
	 * @param boolean $createToken 是否只返回Token，默认为false。
	 * @return string 如果$createToken=true 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->getCatalog();
	/**
	 * 上传单个文件到贴图库 
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#upload}
	 *
	 * @access public
	 * @param int $aid 相册ID
	 * @param array $file 上传的文件。
	 * @return string 如果$file!=null 返回请求接口的json数据否则只返回Token
	 */
	// if(isset($_FILES['file'])){
	// 	if(is_array($_FILES['file']['tmp_name'])){
	// 		foreach ($_FILES['file']['tmp_name'] as $k => $v) {
	// 			if(!empty($v)){
	// 				$res=$ttk->uploadFile('你的相册ID',$v);
	// 				dump(json_decode($res));
	// 			} 
	// 		}
	// 	}else{
	// 		$res=$ttk->uploadFile('相册ID',$_FILES['file']['tmp_name']);
	// 		dump(json_decode($res));
	// 	}
	// }else{
	// 	$res=$ttk->uploadFile('你的相册ID');
	// 	dump($res);
	// }
	/**
	 * 上传多个文件到贴图库 
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#upload}
	 *
	 * @access public
	 * @param int $aid 相册ID
	 * @param string $filename 文件域名字
	 * @return string 如果$file!=null 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->curlUpFile('你的相册ID','file');
	/**
	 * 上传网络文件到贴图库 (只支持单个连接)
	 *
	 * 对应API：{@link http://open.tietuku.cn/doc#upload-url}
	 *
	 * @access public
	 * @param int $aid 相册ID
	 * @param string $fileurl 网络图片地址
	 * @return string 如果$fileurl!=null 返回请求接口的json数据否则只返回Token
	 */
	//$res=$ttk->uploadFromWeb('你的相册ID','网络图片地址');
	dump(($res));
//比var_dump更友好的格式化输出 从 ThinkPHP 提取
function dump($var, $echo=true, $label=null, $strict=true) {
    $label = ($label === null) ? '' : rtrim($label) . ' ';
    if (!$strict) {
        if (ini_get('html_errors')) {
            $output = print_r($var, true);
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
        } else {
            $output = $label . print_r($var, true);
        }
    } else {
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        if (!extension_loaded('xdebug')) {
            $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
        }
    }
    if ($echo) {
        echo($output);
        return null;
    }else
        return $output;
}
?>